const calcFibo = n => (n < 2) ? n : calcFibo(n-1) + calcFibo(n-2)

  self.addEventListener('message', message => {
    console.log('worker get data from index.js')
    const {data} = message
    self.postMessage(calcFibo(data))
  })

