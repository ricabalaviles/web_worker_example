document.addEventListener('DOMContentLoaded', () => {


  document.getElementById('form').addEventListener('submit', (e) => {
    e.preventDefault()
    const result = document.getElementById('result')
    const number = Number(document.getElementById('input').value)

    const taskMessage = document.getElementById('taskMessage')
    const resultMessage = document.getElementById('resultMessages')
    taskMessage.innerText = ''
    resultMessage.innerText = ''

    const worker = new Worker('./worker.js')
    worker.postMessage(number)
    taskMessage.innerText = 'Task sent to a worker'
    worker.onmessage = (message) => {

      result.innerText = `Result: ${message.data}`
      console.log('index.js get result of calculation from worker')
      resultMessage.innerText = 'Result recieved from worker'

      worker.terminate();
    }

  })

})


